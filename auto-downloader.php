<?php
$QS = $_SERVER["QUERY_STRING"];
$QSEscaped = htmlentities($QS);

header("content-type: text/html");
?>
<html><head><title>Auto Download Manager Lite</title><style>
    footer {
        position: absolute;
        bottom:0;
        width:100%;
        text-align:center;
        font-size: large;
        
    }
    #downloadLink {
        background-color: black;
        color: white;
    }
    body {
        text-align: center;
        margin: 0;
        padding-top: 75px;
    }
    p {
        padding: 5px;
    }
</style>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
</head><body>
<?php if (!file_exists($QS) and strlen($QS) >= 1) { ?>
    <h1>404 - File does not exist</h1>
    <p>The file you requested for download does not exist. Please check the URL and try again.</p>
    <?php echo "<p>Requested File: {$QSEscaped}</p>"; ?>

<?php } elseif (strlen($QS) >= 1) { ?>
    <h1>Downloading File: <?php echo $QSEscaped; ?></h1>
    <p>Your file will be automitically downloaded in a second, if it doesnt for some reason, click the link below to manually download it.</p>
    <?php echo "<a class='btn btn-default' id='downloadLink' href='{$QSEscaped}' download='{$QSEscaped}'>Click here to manually download file</a>"?>
    <script>//document.getElementById("downloadLink").click();</script>
    
<?php }  else { ?>
    <h1>Querystring Error</h1>
    <p>There is an error with the provided querystring <?php echo $QSEscaped; ?>. Please try again.</p>
    <?php } ?>
    
<footer>Auto Download Manager Lite powered by <a href="//theorangeone.net">TheOrangeOne</a></footer>    
</body></html>