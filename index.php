<?php
require "property-manager.php"; // Initialise properties
require "file-information.php";

$properties = load_properties();
$file_name = $_GET[$properties["file QS"]] or die("No querystring given.");
if (file_exists($file_name)) {$GLOBALS["file"] = generate_file_object($file_name);}
else {die("File '$file_name' does not exist!");}
header("Content Type: text/html");
?>
<!DOCTYPE html>
<html><head><title><?php echo($properties["html title"]);?></title>
<link rel=stylesheet type=text/css href="style.css"/>
<?php yandex_meta($properties["yandex transparency"]) ?>
</head><body><center>
	<h1>File Information</h1><table border=1>
	<tr><td>File Name:</td><td><?php echo $file["file name"];?></td></tr>
	<tr><td>Description:</td><td><?php echo $file["description"]; ?></td></tr>
	</table>
	<?php echo "<a id='downloadButton' href='./downloads/" . $file["file name"] . "' download='" . $file["file name"] . "' >Download File</a>"; ?>
	<footer>Powered by <a href="//theorangeone.net">TheOrangeOne</a>'s Download Manager</footer>
</center>
<?php
if (isset($_GET['auto'])){
	echo "<script>document.getElementById('downloadButton').click();</script>";
}
?></body></html>