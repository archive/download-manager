<?php
define("PROPERTIES_FILE", "private/properties.yml");

function load_properties(){
	$property_file = fopen(PROPERTIES_FILE, "r") or die("Unable to open properties file!");
	$YAML = fread($property_file, filesize(PROPERTIES_FILE));
	fclose($property_file);
	return yaml_parse($YAML);
}

function yandex_meta($properties) {
	foreach ($properties as &$value){
		if ($value != "fade"){
			$value = "#" . $value;
		}
	}
	echo vsprintf("<meta name=\"viewport\" content=\"ya-title=%s,ya-dock=%s\" />", $properties);
}
?>