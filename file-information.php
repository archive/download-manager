<?php
define("INFORMATION_FILE", "private/information.yml");

function generate_file_object($file_name) {
	$information_file = fopen(INFORMATION_FILE, "r") or die("Unable to open information file!");
	$information = yaml_parse(fread($information_file, filesize(INFORMATION_FILE)))[strtolower($file_name)];	
	$file = array(
		"file name" => $file_name,
		"name" => $information["name"],
		"description" => $information["description"]
	);
	return $file;
}

?>